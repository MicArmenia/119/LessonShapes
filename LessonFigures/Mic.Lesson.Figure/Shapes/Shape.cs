﻿using System;

namespace Mic.Lesson.Figure.Shapes
{
    abstract class Shape : IDrawable
    {
        protected Shape()
        {
            Color = ConsoleColor.White;
        }

        public virtual byte Width { get; set; }
        public virtual byte Height { get; set; }

        public ConsoleColor Color { get; set; }

        //public virtual void Draw() { }
        public abstract void Draw();
    }
}