﻿namespace Mic.Lesson.Figure.Shapes.Rectangles
{
    class Square : Rectangle
    {
        private byte height;
        public sealed override byte Height
        {
            get => height;
            set
            {
                height = value;
                width = value;
            }
        }

        private byte width;
        public sealed override byte Width
        {
            get => width;
            set
            {
                width = value;
                height = value;
            }
        }
    }
}