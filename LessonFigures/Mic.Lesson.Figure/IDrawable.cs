﻿using System;

namespace Mic.Lesson.Figure
{
    interface IDrawable
    {
        ConsoleColor Color { get; set; }
        void Draw();
    }
}