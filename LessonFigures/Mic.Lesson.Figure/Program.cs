﻿using Mic.Lesson.Figure.Shapes;
using Mic.Lesson.Figure.Shapes.Rectangles;
using Mic.Lesson.Figure.Tables;
using System;

namespace Mic.Lesson.Figure
{
    class Program
    {
        static void Main(string[] args)
        {
            Shape l = new Rectangle();
            l.Width = 12;
            l.Height = 16;
            l.Color = ConsoleColor.Yellow;
            IDrawable d1 = l;
            //l.Draw();
            Print(l);

            Console.WriteLine();

            var table = new Table { Columns = 5, Rows = 10 };
            table.Color = ConsoleColor.Red;
            IDrawable d2 = table;
            //table.Draw();
            Print(table);

            Console.ReadLine();
        }

        static void Print(IDrawable figure)
        {
            string symbolstext = new string('-', 6);
            string headerText = $"{symbolstext} {figure.GetType().Name} {symbolstext}";
            Console.WriteLine(headerText);
            Console.WriteLine();

            ConsoleColor defaultColor = Console.ForegroundColor;
            Console.ForegroundColor = figure.Color;

            figure.Draw();

            Console.ForegroundColor = defaultColor;

            Console.WriteLine();
            Console.WriteLine(new string('-', headerText.Length));
        }

        //static void Print(Shape shape)
        //{
        //    string symbolstext = new string('-', 6);
        //    string headerText = $"{symbolstext} {shape.GetType().Name} {symbolstext}";
        //    Console.WriteLine(headerText);
        //    Console.WriteLine();

        //    shape.Draw();

        //    Console.WriteLine();
        //    Console.WriteLine(new string('-', headerText.Length));
        //}

        //static void Print(Table table)
        //{
        //    string symbolstext = new string('-', 6);
        //    string headerText = $"{symbolstext} {table.GetType().Name} {symbolstext}";
        //    Console.WriteLine(headerText);
        //    Console.WriteLine();

        //    table.Draw();

        //    Console.WriteLine();
        //    Console.WriteLine(new string('-', headerText.Length));
        //}
    }
}
