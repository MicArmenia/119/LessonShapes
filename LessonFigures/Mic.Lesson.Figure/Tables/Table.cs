﻿using System;

namespace Mic.Lesson.Figure.Tables
{
    class Table : IDrawable
    {
        public Table()
        {
            columns = 2;
            rows = 2;
            Color = ConsoleColor.White;
        }

        private int columns;
        public int Columns
        {
            set
            {
                if (value < 2)
                    value = 2;
                else
                    columns = value;
            }
        }

        private int rows;
        public int Rows
        {
            set
            {
                if (value < 2)
                    value = 2;
                else
                    rows = value;
            }
        }

        public ConsoleColor Color { get; set; }

        public void Draw()
        {
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < columns; j++)
                    Console.Write("_________|");

                Console.WriteLine();
            }
        }
    }

}